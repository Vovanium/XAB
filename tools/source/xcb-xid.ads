package XCB.XID is

	type XIDType_Tag is new XML.XML_Element_No_Content with private;

	procedure Attribute (
		Context : in out XIDType_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value);

	procedure End_Element (
		Context : in out XIDType_Tag);

	--

	type XIDUnion_Tag is new XML.XML_Element with private;

	procedure Attribute (
		Context : in out XIDUnion_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value);

	procedure Begin_Content (
		Context : in out XIDUnion_Tag);

	function Tag (
		Context : in out XIDUnion_Tag;
		Name : XML.Tag_Name)
		return XML.XML_Element'Class;

private
	type XIDType_Tag is new XML.XML_Element_No_Content with record
		Name : Unbounded_String;
	end record;

	type XIDUnion_Tag is new XML.XML_Element with record
		Name : Unbounded_String;
	end record;

end XCB.XID;
