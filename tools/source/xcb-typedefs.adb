with Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
use  Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
with Ada.Characters.Latin_1;
use  Ada.Characters.Latin_1;

package body XCB.Typedefs is

	procedure Attribute (
		Context : in out Typedef_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value)
	is
	begin
		if Name = "newname" then
			Context.Set_Name (Sane_Identifier (Value));
		elsif Name = "oldname" then
			Context.Ref_Type := Data_Type_Named (Sane_Identifier (Value));
		else
			raise XML.Format_Error with "Unknown <typedef> attribute " & Name;
		end if;
	end Attribute;

	procedure End_Element (
		Context : access Typedef_Tag)
	is
		type DTTA is access all Data_Type_Tag;
	begin
		New_Line;
		Put_Line (HT & "subtype " & Context.Name & " is " & Context.Ref_Type.Name & ";");
		DTTA (Context).End_Element;
	end End_Element;

end XCB.Typedefs;