with Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
use  Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
with Ada.Characters.Latin_1;
use  Ada.Characters.Latin_1;
with Interfaces;
use  Interfaces;

package body XCB.Enum is

	type Item_Tag is new XML.XML_Element with record
		Enum_Name : Unbounded_String;
		Name      : Unbounded_String;
	end record;

	procedure Attribute (
		Context : in out Item_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value);

	function Tag (
		Context : in out Item_Tag;
		Name    : XML.Tag_Name)
		return    XML.XML_Element'Class;

	--

	type Value_Tag is new XML.XML_Element_No_Tags with record
		Enum_Name : Unbounded_String;
		Name      : Unbounded_String;
	end record;

	procedure Plain_Text (
		Context : in out Value_Tag;
		Data    :        String);

	--

	type Bit_Tag is new XML.XML_Element_No_Tags with record
		Enum_Name : Unbounded_String;
		Name      : Unbounded_String;
	end record;

	procedure Plain_Text (
		Context : in out Bit_Tag;
		Data    :        String);

	--

	procedure Attribute (
		Context : in out Enum_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value)
	is
	begin
		if Name = "name" then
			Context.Name := To_Unbounded_String (Sane_Identifier (Value));
		else
			Put_Line (Standard_Error, "Unknown <enum> attribute " & Name);
		end if;
	end Attribute;

	procedure Begin_Content (
		Context : in out Enum_Tag)
	is
	begin
		New_Line;
		Put_Line (HT & "type " & Context.Name & " is new Unsigned_32;");
	end Begin_Content;

	procedure End_Content (
		Context : in out Enum_Tag)
	is
	begin
		Put_Line (HT & "-- end of " & Context.Name);
	end End_Content;

	function Tag (
		Context : in out Enum_Tag;
		Name    : XML.Tag_Name)
		return    XML.XML_Element'Class
	is
	begin
		if Name = "item" then
			return Item_Tag'(Enum_Name => Context.Name, others => <>);
		end if;
		Put_Line (Standard_Error, "Unknown tag in <enum> " & Name);
		return R : XML.Void_Context do
			null;
		end return;
	end Tag;

	--

	procedure Attribute (
		Context : in out Item_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value)
	is
	begin
		if Name = "name" then
			Context.Name := To_Unbounded_String (Sane_Identifier (Value));
		else
			Put_Line (Standard_Error, "Unknown <item> attribute " & Name);
		end if;
	end Attribute;

	function Tag (
		Context : in out Item_Tag;
		Name    : XML.Tag_Name)
		return    XML.XML_Element'Class
	is
	begin
		if Name = "value" then
			return Value_Tag'(Name => Context.Name, Enum_Name => Context.Enum_Name);
		elsif Name = "bit" then
			return Bit_Tag'(Name => Context.Name, Enum_Name => Context.Enum_Name);
		end if;
		Put_Line (Standard_Error, "Unknown tag in <item> " & Name);
		return R : XML.Void_Context do
			null;
		end return;
	end Tag;

	--

	procedure Plain_Text (
		Context : in out Value_Tag;
		Data    :        String)
	is
		Val : Unsigned_32 := Unsigned_32'Value (Data);
	begin
		Put_Line (HT & Context.Enum_Name & "_" & Context.Name
		& " : constant " & Context.Enum_Name & " := "
		& Unsigned_32'Image (Val) & ";");
	end Plain_Text;

	--

	procedure Plain_Text (
		Context : in out Bit_Tag;
		Data    :        String)
	is
		Val : Unsigned_32 := Unsigned_32'Value (Data);
	begin
		Put_Line (HT & Context.Enum_Name & "_" & Context.Name
		& " : constant " & Context.Enum_Name & " := 2 **"
		& Unsigned_32'Image (Val) & ";");
	end Plain_Text;


end XCB.Enum;