with XCB.Data_Types;
use  XCB.Data_Types;

package XCB.Structs is

	type Struct_Tag is new Data_Type and XML.XML_Element with private;

	procedure Attribute (
		Context : in out Struct_Tag;
		Name    :        XML.Attribute_Name;
		Value   :        XML.Attribute_Value);
	procedure Begin_Content (
		Context : in out Struct_Tag);
	procedure End_Content (
		Context : in out Struct_Tag);
	function Tag (
		Context : in out Struct_Tag;
		Name    :        XML.Tag_Name)
		return    in out XML.XML_Element'Class;

private

	type Struct_Tag is new Data_Type and XML.XML_Element with record
		C_Name    : Unbounded_String;
		Pad_Count : Natural := 0;
		Closed    : Boolean := False;
	end record;

end XCB.Structs;
