with Ada.Containers.Indefinite_Ordered_Sets;

package XCB.Data_Types is

	type Byte_Offset is new Integer;
	subtype Byte_Count is Byte_Offset range 0 .. Byte_Offset'Last;

	type Data_Type is abstract tagged null record;

	function Name (T : Data_Type) return Identifier is abstract;

	-- Data Type base

	function Key (T : Data_Type'Class) return Identifier is (T.Name);
	function Order (A, B : Data_Type'Class) return Boolean is (A.Name < B.Name);

	package Data_Type_Sets is new Ada.Containers.Indefinite_Ordered_Sets (
		Element_Type => Data_Type'Class,
		"<"          => Order);

	package Data_Type_Set_Keys is new Data_Type_Sets.Generic_Keys (
		Key_Type => Identifier,
		Key      => Key);

	Data_Type_Base : Data_Type_Sets.Set;

	procedure Register (T : Data_Type'Class);

end XCB.Data_Types;
