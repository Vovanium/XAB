with Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
use  Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
with Ada.Strings.Unbounded;
use  Ada.Strings.Unbounded;
with XML;
with XCB;
with XCB.Data_Types;

--
-- Parses XCB's XML file and convert it to Ada package
-- It reads standard input, writes to standard output and logs to standard error, nothing more.
--
procedure XML_to_Ada is
	Root : XCB.XCB_File;
	use type XML.XML_Unit_Kind;
	use XML.String_Vectors;
	use XML.String_Maps;
begin
	loop
		declare
			U : XML.XML_Unit := XML.Get_Unit;
		begin
			Put(Ada.Text_IO.Count'Image (U.Start_Line)
			& ":" & Ada.Text_IO.Count'Image (U.Start_Col) & "  "
			& XML.XML_Unit_Kind'Image (U.Kind));
			case U.Kind is
			when XML.Opening_Tag | XML.Closing_Tag | XML.Special =>
				Put (" """ & U.Name & """");
				if U.Closed then
					Put (" CLOSED");
				end if;
			when others =>
				null;
			end case;
			New_Line;
			case U.Kind is
			when XML.Opening_Tag | XML.Closing_Tag | XML.Special =>
				for J in U.Attributes.Iterate loop
					Put_Line ("  " & Key (J) & "=""" & Element (J) & """");
				end loop;
			when XML.Plain_Text | XML.CDATA =>
				for J of U.Lines loop
					Put_Line ("  """ & J & """");
				end loop;
			when others =>
				null;
			end case;
			exit when U.Kind = XML.End_of_XML;
		end;
	end loop;
	--XML.Parse_XML_Content (Root);
	--for J of XCB.Data_Types.Data_Type_Base loop
	--	Put_Line (Standard_Error, "registered type: " & J.Name);
	--end loop;

end XML_to_Ada;