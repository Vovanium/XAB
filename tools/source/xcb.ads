with XML;
with Ada.Strings.Unbounded;
use  Ada.Strings.Unbounded;
package XCB is

	subtype Identifier is String;

	type XCB_File is new XML.XML_Element with null record;

	procedure Attribute     (Context : in out XCB_File; Name : XML.Attribute_Name; Value : XML.Attribute_Value);
	function  Tag           (Context : in out XCB_File; Name : XML.Tag_Name) return XML.XML_Element'Class;
	procedure Plain_Text    (Context : in out XCB_File; Data : String);

	--

	type XCB_Tag is new XML.XML_Element with private;

	procedure Attribute     (Context : in out XCB_Tag; Name : XML.Attribute_Name; Value : XML.Attribute_Value);
	procedure End_Element   (Context : in out XCB_Tag);
	function  Tag           (Context : in out XCB_Tag; Name : XML.Tag_Name) return XML.XML_Element'Class;
	procedure Plain_Text    (Context : in out XCB_Tag; Data : String);

	-- Sanitize Ada identifier
	function Sane_Identifier (S : String) return Identifier;

	function C_Identifier (S : String) return String;

private
	type XCB_Tag is new XML.XML_Element with record
		Package_Name : Unbounded_String := Null_Unbounded_String;
		Header_Put : Boolean := False;
	end record;
end XCB;