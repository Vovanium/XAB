with Ada.Text_IO;
with Ada.Strings.Unbounded;
use  Ada.Strings.Unbounded;
with Ada.Containers.Indefinite_Vectors;
with Ada.Containers.Indefinite_Ordered_Maps;

-- Simple XML parser

package XML is

	Format_Error : exception;

	subtype Tag_Name is String;
	subtype Attribute_Name is String;
	subtype Attribute_Value is String;

	package String_Vectors is new Ada.Containers.Indefinite_Vectors (
		Index_Type   => Natural,
		Element_Type => String);
	package String_Maps is new Ada.Containers.Indefinite_Ordered_Maps (
		Key_Type     => String,
		Element_Type => String);

	type XML_Unit_Kind is (
		Plain_Text,
		CDATA,
		Comment,
		Opening_Tag,
		Closing_Tag,
		Special,
		End_of_XML);

	type XML_Unit (Kind : XML_Unit_Kind) is record
		Start_Line,
		Start_Col,
		End_Line,
		End_Col   : Ada.Text_IO.Count := Ada.Text_IO.Count'First;
		case Kind is
		when Plain_Text | CDATA | Comment =>
			Lines      : String_Vectors.Vector := String_Vectors.Empty_Vector;
		when Opening_Tag | Closing_Tag | Special =>
			Name       : Unbounded_String := Null_Unbounded_String;
			Attributes : String_Maps.Map  := String_Maps.Empty_Map;
			Closed     : Boolean          := False;
		when End_Of_XML =>
			null;
		end case;
	end record;

	function Get_Unit return XML_Unit;

	type XML_Element is interface;
	-- Defines actions on

	procedure Set_Name      (Context : in out XML_Element; Name : Tag_Name) is null;
	procedure Attribute     (Context : in out XML_Element; Name : Attribute_Name; Value : Attribute_Value) is null;
	procedure No_Content    (Context : in out XML_Element) is null;
	procedure Begin_Content (Context : in out XML_Element) is null;
	procedure End_Content   (Context : in out XML_Element) is null;
	procedure End_Element   (Context : in out XML_Element) is null;
	function  Tag           (Context : in out XML_Element; Name : Tag_Name) return XML_Element'Class is abstract;
	procedure Element       (Context : in out XML_Element; Data : XML_Element'Class) is null;
	procedure Plain_Text    (Context : in out XML_Element; Data : String) is null;
	procedure Begin_CDATA   (Context : in out XML_Element) is null;
	procedure End_CDATA     (Context : in out XML_Element) is null;
	
	procedure Parse_XML_Content (Context : in out XML_Element'Class);

	--

	type XML_Element_No_Tags is abstract new XML_Element with null record;

	function Tag (
		Context : in out XML_Element_No_Tags;
		Name : Tag_Name)
		return XML_Element'Class is (raise Format_Error with "no tags allowed");

	--

	type XML_Element_No_Content is abstract new XML_Element_No_Tags with null record;

	procedure Begin_Content (Context : in out XML_Element_No_Content);

	--

	type Void_Context is new XML_Element with private;
	-- Type not accepting any data

	procedure Set_Name     (Context : in out Void_Context; Name : Tag_Name);
	procedure Attribute    (Context : in out Void_Context; Name : Attribute_Name; Value : Attribute_Value);
	function  Tag          (Context : in out Void_Context; Name : Tag_Name) return XML_Element'Class;
	procedure Plain_Text   (Context : in out Void_Context; Data : String);

	type Ignored_Context is new XML_Element with null record;
	function  Tag          (Context : in out Ignored_Context; Name : Tag_Name) return XML_Element'Class;

private
	type Void_Context is new XML_Element with record
		Level : Natural := 1;
	end record;
end XML;