package XCB.Enum is

	type Enum_Tag is new XML.XML_Element with private;

	procedure Attribute (
		Context : in out Enum_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value);

	procedure Begin_Content (
		Context : in out Enum_Tag);

	procedure End_Content (
		Context : in out Enum_Tag);

	function Tag (
		Context : in out Enum_Tag;
		Name    : XML.Tag_Name)
		return    XML.XML_Element'Class;

private

	type Enum_Tag is new XML.XML_Element with record
		Name : Unbounded_String;
	end record;

end XCB.Enum;