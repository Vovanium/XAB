with Ada.Text_IO;
use  Ada.Text_IO;
with Ada.Characters.Handling;
use  Ada.Characters.Handling;

with XCB.Enum;
with XCB.Struct;
with XCB.XID;
--
-- Parser of XCB file schema
--

package body XCB is

	procedure Attribute (Context : in out XCB_File; Name : XML.Attribute_Name; Value : XML.Attribute_Value) is
	begin
		raise XML.Format_Error with "non root context";
	end Attribute;

	function Tag (Context : in out XCB_File; Name : XML.Tag_Name) return XML.XML_Element'Class is
	begin
		if Name = "xcb" then
			return R : XCB_Tag do
				null;
			end return;
		else
			raise XML.Format_Error with "wrong root";
		end if;
	end Tag;

	procedure Plain_Text (Context : in out XCB_File; Data : String) is
	begin
		null;
	end Plain_Text;

	--

	procedure Attribute (Context : in out XCB_Tag; Name : XML.Attribute_Name; Value : XML.Attribute_Value) is
	begin
		if Name = "header" then
			Context.Package_Name := To_Unbounded_String ("XAB." & Value);
		else
			Put_Line (Standard_Error, "Unknown <xcb> attribute " & Name);
		end if;
	end Attribute;

	procedure End_Element (Context : in out XCB_Tag) is
	begin
		if Context.Header_Put then
			Put_Line ("end " & To_String (Context.Package_Name) & ";");
		end if;
	end End_Element;

	function Tag (Context : in out XCB_Tag; Name : XML.Tag_Name) return XML.XML_Element'Class is
	begin
		if  Name = "doc" then
			return R : XML.Ignored_Context do
				null;
			end return;
		end if;

		if not Context.Header_Put then
			Put_Line ("package " & To_String (Context.Package_Name) & " is");
			Context.Header_Put := True;
		end if;
		if Name = "enum" then
			return R : Enum.Enum_Tag do
				null;
			end return;
		elsif Name = "struct" then
			return R : Struct.Struct_Tag do
				null;
			end return;
		elsif Name = "xidtype" then
			return R : XID.XIDType_Tag do
				null;
			end return;
		elsif Name = "xidunion" then
			return R : XID.XIDUnion_Tag do
				null;
			end return;
		end if;
		return R : XML.Void_Context do
			null;
		end return;
	end Tag;

	procedure Plain_Text (Context : in out XCB_Tag; Data : String) is
	begin
		null;
	end Plain_Text;

	function Sane_Identifier (S : String) return String is
	begin
		return S;
	end;

	function C_Identifier (S : String) return String is
		T : String := S;
	begin
		for J in T'Range loop
			T (J) := To_Lower (T (J));
		end loop;
		return T;
	end;

end XCB;
