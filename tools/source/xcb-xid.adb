with Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
use  Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
with Ada.Characters.Latin_1;
use  Ada.Characters.Latin_1;

package body XCB.XID is

	type Type_Tag is new XML.XML_Element_No_Tags with null record;

	procedure Plain_Text (Context : in out Type_Tag; Data : String);

	--

	procedure Attribute (
		Context : in out XIDType_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value)
	is
	begin
		if Name = "name" then
			Context.Name := To_Unbounded_String (Sane_Identifier (Value));
		else
			Put_Line (Standard_Error, "Unknown <xidtype> attribute " & Name);
		end if;
	end Attribute;

	procedure End_Element (
		Context : in out XIDType_Tag)
	is
	begin
		New_Line;
		Put_Line (HT & "subtype " & Context.Name & " is XID_Type;");
	end End_Element;

	--

	procedure Attribute (
		Context : in out XIDUnion_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value)
	is
	begin
		if Name = "name" then
			Context.Name := To_Unbounded_String (Sane_Identifier (Value));
		else
			Put_Line (Standard_Error, "Unknown <xidunion> attribute " & Name);
		end if;
	end Attribute;

	procedure Begin_Content (
		Context : in out XIDUnion_Tag)
	is
	begin
		New_Line;
		Put_Line (HT & "subtype " & Context.Name & " is XID_Type;");
	end Begin_Content;

	function Tag (
		Context : in out XIDUnion_Tag;
		Name    : XML.Tag_Name)
		return    XML.XML_Element'Class
	is
	begin
		if Name = "type" then
			return R : Type_Tag do
				null;
			end return;
		end if;
		Put_Line (Standard_Error, "Unknown tag in <xidunion> " & Name);
		return R : XML.Void_Context do
			null;
		end return;
	end Tag;

	--
	procedure Plain_Text (Context : in out Type_Tag; Data : String) is
	begin
		Put_Line (HT & "-- or " & Data);
	end Plain_Text;

end XCB.XID;
