package body XCB.Data_Types is

	procedure Register (T : Data_Type'Class) is
	begin
		Data_Type_Base.Insert (T);
	end Register;

end XCB.Data_Types;
