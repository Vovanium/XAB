with XCB.Data_Types;
use  XCB.Data_Types;

package XCB.Typedefs is

	type Typedef_Tag is new Data_Type_Tag with private;

	procedure Attribute (
		Context : in out Typedef_Tag;
		Name    :        XML.Attribute_Name;
		Value   :        XML.Attribute_Value);

	procedure End_Element (
		Context : in out Typedef_Tag);

	function Tag (
		Context : in out Typedef_Tag;
		Name    :        XML.Tag_Name)
		return           XML.XML_Element'Class is (raise XML.Format_Error with "no tags allowed");

private
	type Typedef_Tag is new Data_Type_Tag with record
		Ref_Type : Data_Type_Access := null;
	end record;

end XCB.Typedefs;