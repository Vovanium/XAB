with Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
use  Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
with Ada.Strings.Fixed;
use  Ada.Strings.Fixed;
with Ada.Characters.Latin_1;
use  Ada.Characters.Latin_1;

--
-- Schema for <struct> tags
--
package body XCB.Struct is

	type Field_Tag is new XML.XML_Element_No_Content with record
		Kind,
		Name,
		Enum : Unbounded_String;
	end record;

	procedure Attribute (
		Context : in out Field_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value);
	procedure End_Element (
		Context : in out Field_Tag);

	--

	type List_Tag (Struct : not null access Struct_Tag) is new XML.XML_Element with record
		Kind,
		Name,
		C_Name,
		Length_Ref : Unbounded_String;
	end record;

	procedure Attribute (
		Context : in out List_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value);

	procedure End_Element (
		Context : in out List_Tag);

	function Tag (
		Context : in out List_Tag;
		Name    : XML.Tag_Name)
		return    XML.XML_Element'Class;

	--

	--type Fieldref_Tag (List : not null access List_Tag) is new XML.XML_Element_No_Tags with null record;

	--procedure Plain_Text (
	--	Context : in out Fieldref_Tag;
	--	Data    :        String);

	--

	type Pad_Tag (Struct : not null access Struct_Tag) is new XML.XML_Element_No_Content with record
		Id    : Natural := 0;
		Bytes : Positive := 1;
	end record;

	procedure Attribute (
		Context : in out Pad_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value);
	procedure End_Element (
		Context : in out Pad_Tag);

	--

	function Name (T : Struct_Tag) return Identifier is (To_String (T.Name));

	procedure Attribute (
		Context : in out Struct_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value)
	is
	begin
		if Name = "name" then
			Context.Name := To_Unbounded_String (Sane_Identifier (Value));
			Context.C_Name := To_Unbounded_String (C_Identifier (Value));
		else
			raise XML.Format_Error with "Unknown <struct> attribute " & Name;
		end if;
	end Attribute;

	procedure Begin_Content (Context : in out Struct_Tag) is
	begin
		New_Line;
		Put_Line (HT & "type " & Context.Name & " is record");
	end Begin_Content;

	procedure Close (Context : in out Struct_Tag) is
	begin
		if not Context.Closed then
			Context.Closed := True;
			Put_Line (HT & "end record with Convention => C;");
		end if;
	end Close;

	procedure End_Content (Context : in out Struct_Tag) is
	begin
		Close (Context);
	end End_Content;

	procedure End_Element (Context : in out Struct_Tag) is
	begin
		Data_Types.Register (Context);
	end End_Element;

	function Tag (
		Context : in out Struct_Tag;
		Name    : XML.Tag_Name)
		return    XML.XML_Element'Class
	is
	begin
		if Name = "field" then
			return R : Field_Tag do
				null;
			end return;
		elsif Name = "pad" then
			return R : Pad_Tag (Context'Access) do
				R.Id := Context.Pad_Count;
				Context.Pad_Count := Context.Pad_Count + 1;
			end return;
		elsif Name = "list" then
			Close (Context);
			return R : List_Tag (Context'Access) do
				null;
			end return;
		end if;
		raise XML.Format_Error with "Unknown tag in <struct> " & Name;
	end Tag;

	--

	procedure Attribute (
		Context : in out Field_Tag;
		Name : XML.Attribute_Name;
		Value : XML.Attribute_Value)
	is
	begin
		if Name = "type" then
			Context.Kind := To_Unbounded_String (Sane_Identifier (Value));
		elsif Name = "name" then
			Context.Name := To_Unbounded_String (Sane_Identifier (Value));
		elsif Name = "enum" or else Name = "mask" then
			if Context.Enum /= "" then
				raise XML.Format_Error with "cannot combine enum and mask";
			end if;
			Context.Enum := To_Unbounded_String (Sane_Identifier (Value));
		else
			raise XML.Format_Error with "Unknown <field> attribute " & Name;
		end if;
	end Attribute;

	procedure End_Element (Context : in out Field_Tag) is
	begin
		if Context.Enum /= "" then
			Put_Line (HT & HT & Context.Name & " : " & Context.Kind &
			"; -- " & Context.Enum);
			-- TODO: need someway make it type safe
		else
			Put_Line (HT & HT & Context.Name & " : " & Context.Kind & ";");
		end if;
	end End_Element;

	--

	procedure Attribute (
		Context : in out List_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value)
	is
	begin
		if Name = "type" then
			Context.Kind := To_Unbounded_String (Sane_Identifier (Value));
		elsif Name = "name" then
			Context.Name := To_Unbounded_String (Sane_Identifier (Value));
			Context.C_Name := To_Unbounded_String (C_Identifier (Value));
		else
			raise XML.Format_Error with "Unknown <list> attribute " & Name;
		end if;
	end Attribute;

	function Tag (
		Context : in out List_Tag;
		Name    : XML.Tag_Name)
		return    XML.XML_Element'Class
	is
	begin
		return R : XML.Ignored_Context do -- currently do not analyse it, rely on XCB
			null;
		end return;
		--raise XML.Format_Error with "Unknown tag in <list> " & Name;
	end Tag;

	procedure End_Element (
		Context : in out List_Tag)
	is
		Name : Unbounded_String := Context.Struct.Name & "_" & Context.Name;
		C_Name : Unbounded_String := "xcb_" & Context.Struct.C_Name & "_" & Context.C_Name;
	begin
		New_Line;
		Put_Line (HT & "function " & Name & " (");
		Put_Line (HT & HT & "R    : access " & Context.Struct.Name & ")");
		Put_Line (HT & HT & "return access " & Context.Kind);
		Put_Line (HT & "with Import, Convention => C, External_Name => """ & C_Name & """;");
		New_Line;
		Put_Line (HT & "function " & Name & "_Length (");
		Put_Line (HT & HT & "R    : access " & Context.Struct.Name & ")");
		Put_Line (HT & HT & "return C.int ");
		Put_Line (HT & "with Import, Convention => C, External_Name => """ & C_Name & "_length"";");
	end End_Element;

	--

	--procedure Plain_Text (
	--	Context : in out Fieldref_Tag;
	--	Data    :        String)
	--is
	--begin
	--	Context.List.Length_Ref := To_Unbounded_String (Data);
	--end Plain_Text;

	--

	procedure Attribute (
		Context : in out Pad_Tag;
		Name    : XML.Attribute_Name;
		Value   : XML.Attribute_Value)
	is
	begin
		if Name = "bytes" then
			Context.Bytes := Positive'Value (Value);
		elsif Name = "align" then
			if Context.Struct.Closed then
				null; -- no actions yet
			else
				raise Program_Error with "alignment not implemented";
			end if;
		else
			raise XML.Format_Error with "Unknown <pad> attribute " & Name;
		end if;
	end Attribute;

	procedure End_Element (Context : in out Pad_Tag) is
	begin
		Put_Line (HT & HT & "Pad_" & Trim (Natural'Image (Context.Id), Ada.Strings.Both) &
		" : Pad_Array (1 .." &  Natural'Image (Context.Bytes) & ");");
	end End_Element;

end XCB.Struct;
