with Ada.Text_IO;
use  Ada.Text_IO;
with Ada.Strings.Unbounded;
use  Ada.Strings.Unbounded;
with Ada.Characters.Latin_1;
use  Ada.Characters.Latin_1;

package body XML is

	procedure Get_Expect (Ch : Character) is
		Cb : Character;
	begin
		Get (Cb);
		if Cb /= Ch then
			raise Format_Error with """" & Ch & """ /= """ & Cb & """";
		end if;
	end Get_Expect;

	procedure Get_Expect (S : String) is
	begin
		for J of S loop
			Get_Expect (J);
		end loop;
	end Get_Expect;

	procedure Skip_Spaces is
		C  : Character;
		EL : Boolean;
	begin
		Spaces: loop
			Look_Ahead (C, EL);
			if EL then
				exit Spaces when End_of_File;
				Skip_Line;
			else
				exit Spaces when C not in ' ' | HT;
				Get_Expect (C);
			end if;
		end loop Spaces;
	end Skip_Spaces;

	-- Get XML identifier
	procedure Get_Name (S : out Unbounded_String) is
		C  : Character;
		EL : Boolean;
	begin
		S := Null_Unbounded_String;
		GNL: loop
			Look_Ahead (C, EL);
			exit GNL when EL or else (C not in 'A' .. 'Z' | 'a' .. 'z' | ':' | '-' | '_');
			Get (C);
			Append (S, C);
		end loop GNL;
	end;

	-- Get a string enclosed in single or double quotes
	procedure Get_Quoted (S : out Unbounded_String) is
		C, D : Character;
	begin
		S := Null_Unbounded_String;
		Skip_Spaces;
		Get (C);
		if C /= Quotation and C /= Apostrophe then
			raise Format_Error with "quote expected, got """ & C & """";
		end if;
		Quoted: loop
			Get (D);
			exit Quoted when D = C;
			Append (S, D);
		end loop Quoted;
	end Get_Quoted;

	function Get_Unit return XML_Unit is
		C  : Character;
		EL : Boolean;
		SL, SC : Ada.Text_IO.Count;
		S, V : Unbounded_String := Null_Unbounded_String;
		procedure Finally (Unit : in out XML_Unit) is
		begin
			Unit.Start_Line := SL;
			Unit.Start_Col := SC;
			Unit.End_Line := Line (Standard_Input);
			Unit.End_Col := Col (Standard_Input);
		end Finally;
		procedure Get_Verbatim (Unit : in out XML_Unit; SMatch : in String) is
		begin
			Verbatim: loop
				if End_of_Line then
					exit Verbatim when End_of_File;
					Unit.Lines.Append (To_String (S));
					S := Null_Unbounded_String;
					Skip_Line;
				else
					Get (C);
					if C = '>' and then Tail (S, 2) = SMatch then
						S := Head (S, Length (S) - 2);
						exit Verbatim;
					else
						Append (S, C);
					end if;
				end if;
			end loop Verbatim;
			Unit.Lines.Append (To_String (S));
			Finally (Unit);
		end Get_Verbatim;
		procedure Get_Text (Unit : in out XML_Unit) is
		begin
			Txt : loop
				Look_Ahead (C, EL);
				if EL then
					exit Txt when End_of_File;
					Unit.Lines.Append (To_String (S));
					S := Null_Unbounded_String;
					Skip_Line;
					Skip_Spaces;
				else
					if C = '<' then
						exit Txt;
					else
						Get (C);
						Append (S, C);
					end if;
				end if;
			end loop Txt;
			Unit.Lines.Append (To_String (S));
			Finally (Unit);
		end Get_Text;
		procedure Get_Tag (Unit : in out XML_Unit) is
		begin
			Get_Name (S);
			Unit.Name := S;
			Attrs : loop
				Skip_Spaces;
				Look_Ahead (C, EL);
				exit Attrs when EL; -- something strange?
				exit Attrs when C not in 'A' .. 'Z' | 'a' .. 'z';
				Get_Name (S);
				Skip_Spaces;
				Get_Expect ('=');
				Get_Quoted (V);
				Unit.Attributes.Insert (To_String (S), To_String (V));
			end loop Attrs;
			Get (C);
			case C is
			when '>' =>
				null;
			when '?' =>
				if Unit.Kind /= Special then
					raise Format_Error;
				end if;
				Get_Expect ('>');
			when '/' =>
				Unit.Closed := True;
				Get_Expect ('>');
			when others =>
				raise Format_Error;
			end case;
			Finally (Unit);
		end Get_Tag;
	begin
		while End_of_Line loop
			if End_of_File then
				return (
					Kind => End_of_XML,
					Start_Line | End_Line => Line (Standard_Input),
					Start_Col | End_Col => Col (Standard_Input));
			end if;
			Skip_Line;
			Skip_Spaces;
		end loop;
		SL := Line (Standard_Input);
		SC := Col (Standard_Input);

		Look_Ahead (C, EL);

		pragma Assert (not EL);

		if C = '<' then
			Get (C);
			Look_Ahead (C, EL);
			if EL then
				C := NEL;
			end if;
			case C is
			when '!' => -- comment
				Get_Expect ('!');
				Get (C);
				case C is
				when '-' =>
					Get_Expect ("-");
					return U : XML_Unit (Comment) do
						Get_Verbatim(U, "--");
					end return;
				when '[' => -- CDATA
					Get_Expect ("CDATA[");
					return U : XML_Unit (CDATA) do
						Get_Verbatim(U, "]]");
					end return;
				when others =>
					raise Format_Error;
				end case;
			when '?' => -- special
				Get (C);
				return U : XML_Unit (Special) do
					Get_Tag(U);
				end return;
			when '/' => -- closing
				Get (C);
				return U : XML_Unit (Closing_Tag) do
					Get_Tag(U);
				end return;
			when others => -- opening (most probably)
				return U : XML_Unit (Opening_Tag) do
					Get_Tag(U);
				end return;
			end case;
		else
			return U : XML_Unit (Plain_Text) do
				Get_Text(U);
			end return;
		end if;
	exception
	when Format_Error =>
		Put_Line (Standard_Error, "At " & Ada.Text_IO.Count'Image (SL)
		& ":" & Ada.Text_IO.Count'Image (SC)
		& " .. " & Ada.Text_IO.Count'Image (Line (Standard_Input))
		& ":" & Ada.Text_IO.Count'Image (Col (Standard_Input)));
		raise;
	end Get_Unit;

	-- Skip comment after <!-- including trailing -->
	procedure Skip_Comment_Tail is
		C : Character;
	begin
		Comment: loop
			Get (C);
			if C = '-' and not End_of_Line (Standard_Input) then
				Get (C);
				if C = '-' and not End_of_Line (Standard_Input) then
					Get (C);
					if C = '>' then
						exit Comment;
					end if;
				end if;
			end if;
		end loop Comment;
	end Skip_Comment_Tail;

	procedure Parse_XML_Content (Context : in out XML_Element'Class) is
		C    : Character;
		EL   : Boolean;
		Buf  : Unbounded_String;
		Name : Unbounded_String;
		AN   : Unbounded_String;
		AV   : Unbounded_String;

		procedure Flush_Buf is
		begin
			Trim (Buf, Ada.Strings.Both);
			if Length (Buf) > 0 then
				Context.Plain_Text (To_String (Buf));
				Buf := Null_Unbounded_String;
			end if;
		end;

		procedure Get_CDATA_Tail is
		begin
			Buf := Null_Unbounded_String;
			Context.Begin_CDATA;
			Cdata: loop
				while End_Of_Line (Standard_Input) loop
					Flush_Buf;
					Skip_Line (Standard_Input);
				end loop;
				Get (C);
				if C = ']' then
					Get (C);
					if C = ']' then
						Get (C);
						if C = '>' then
							exit Cdata;
						else
							Append (Buf, ']');
							Append (Buf, ']');
							Append (Buf, C);
						end if;
					else
						Append (Buf, ']');
						Append (Buf, C);
					end if;
				else
					Append (Buf, C);
				end if;
			end loop Cdata;
			Context.End_CDATA;
		end Get_CDATA_Tail;

		procedure Get_Special is
		begin
			Special: loop
				Get (C);
				if C = '"' then --"
					Quoted: loop
						Get (C);
						if C = '"' then --"
							exit Quoted;
						end if;
					end loop Quoted;
				elsif C = '?' then
					Get_Expect ('>');
					exit Special;
				end if;
			end loop Special;
		end Get_Special;

	begin
		Main : loop
			while End_of_Line loop
				Flush_Buf;
				Skip_Line;
				if End_of_File then
					Flush_Buf;
					exit Main;
				end if;
			end loop;
			Look_Ahead (C, EL); -- EL should be false now
			case C is
			when '<' =>
				Flush_Buf;
				Get_Expect ('<');
				Look_Ahead (C, EL);
				if EL then
					C := NEL; -- dirty
				end if;
				case C is
				when '!' => -- comment
					Get_Expect ('!');
					Get (C);
					case C is
					when '-' =>
						Get_Expect ('-');
						Skip_Comment_Tail;
					when '[' => -- CDATA
						Get_Expect ("CDATA[");
						Get_CDATA_Tail;
					when others =>
						raise Format_Error with "<!" & C;
					end case;
				when '?' =>
					Get_Expect ('?');
					Get_Special;
				when 'a' .. 'z' | 'A' .. 'Z' => -- tag
					Get_Name (Name);
					declare
						N : String := To_String (Name);
						New_Context : XML_Element'Class := Context.Tag (N);
					begin
						New_Context.Set_Name (N);
						Tag_Attrs: loop
							Look_Ahead (C, EL);
							if EL then
								C := NEL; -- dirty
							end if;
							case C is
							when 'a' .. 'z' | 'A' .. 'Z' =>
								Get_Name (AN);
								Skip_Spaces;
								Get_Expect ('=');
								Get_Quoted (AV);
								New_Context.Attribute (To_String (AN), To_String (AV));
							when ' ' | HT  =>
								Get (C);
							when '/' =>
								Get_Expect ("/>");
								New_Context.No_Content;
								goto Tag_End;
							when '>' =>
								Get_Expect ('>');
								exit Tag_Attrs;
							when others =>
								raise Format_Error with "<tag ... """ & C & """";
							end case;
						end loop Tag_Attrs;

						New_Context.Begin_Content;
						Parse_XML_Content (New_Context);

						Get_Name (AN);
						if AN /= Null_Unbounded_String and then AN /= Name then
							raise Format_Error with "unmatched closing tag : <" &
							To_String (Name) & "> /= <" & To_String (AN) & ">";
						end if;
						Get_Expect ('>');
						New_Context.End_Content;

						<<Tag_End>>
						New_Context.End_Element;
						Context.Element (New_Context);
					end;
				when '/' =>
					Get_Expect ('/');
					exit Main; -- just leave and let code above parse end tag
				when others =>
					Put_Line (Standard_Error, "Sequence: <" & C);
				end case;
			when others =>
				Get (C);
				Append (Buf, C);
			end case;
		end loop Main;
	exception
	when others =>
		Put_Line (Standard_Error, "at" & Positive_Count'Image (Line (Standard_Input)) &
		":" & Positive_Count'Image (Col (Standard_Input)));
		raise;
	end;

	--

	procedure Begin_Content (Context : in out XML_Element_No_Content) is
	begin
		raise Format_Error with "no content allowed here";
	end;

	--

	procedure Set_Name (Context : in out Void_Context; Name : Tag_Name) is
	begin
		Set_Col (Standard_Error, Positive_Count (Context.Level));
		Put_Line (Standard_Error, "Tag: " & Name);
	end;

	procedure Attribute (Context : in out Void_Context; Name : Attribute_Name; Value : Attribute_Value) is
	begin
		--Set_Col (Standard_Error, Positive_Count (Context.Level));
		--Put_Line (Standard_Error, "Attribute: " & Name & "=""" & Value & """");
		null;
	end;

	function Tag (Context : in out Void_Context; Name : Tag_Name) return XML_Element'Class is
	begin
		return R : Void_Context := Context do
			R.Level := R.Level + 1;
		end return;
	end;

	procedure Plain_Text (Context : in out Void_Context; Data : String) is
	begin
		--Set_Col (Standard_Error, Positive_Count (Context.Level));
		--Put_Line (Standard_Error, "Plain Text: """ & Data & """");
		null;
	end Plain_Text;

	--

	function Tag (Context : in out Ignored_Context; Name : Tag_Name) return XML_Element'Class is
	begin
		return R : Ignored_Context := Context do
			null;
		end return;
	end;


end XML;
