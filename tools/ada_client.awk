#!/usr/bin/awk -f
# Converts XML file from xcb-proto to ada source

# Validate type name (existing)
function type_name (n,  F) {
	if(match(n, "CARD([0-9]+)", F)) {
		return "Unsigned_"F[1];
	} else if(match(n, "INT([0-9]+)", F)) {
		return "Integer_"F[1];
	} else {
		return n;
	}

}

function new_type (n) {
	return n;
}

# Validate identifier
function identifier (a) {
	return a;
}

BEGIN {
	print "with Interfaces.C;";
	print "use Interfaces;";
	context = "!";
}

# Multiline comment
/^ *<!--$/, /^ *-->$/ {
	next;
}

# Single line comment
/ *<!-- .* -->$/ {
	next;
}

# XML header
/<?xml version="1.0" encoding="utf-8"?>/ {
	next;
}

# xcb tags
match($0, "^ *<xcb header=\"([a-z0-9_]+)\"(( +[a-z-]+=\"[ A-Za-z0-9-]+\")*)(>)?$", A) {
	package_name = "XCB." A[1];
	print "package " package_name" with Preelaborate is";
	if(A[4] == ">") {
		context = "";
		have_header = 1;
	} else {
		context = "xcb-attr";
	}
	next;
}

context == "xcb-attr" && match($0, "^ *(( +[a-z-]+=\"[ A-Za-z0-9-]+\")*)(>)?$", A) {
	if(A[3] == ">") {
		context = "";
		have_header = 1;
	}
	next;
}

match($0, "^ *</xcb>$", A) {
	print "end " package_name";";
	if(!have_header) {
		print "-- No header";
		exit 1;
	}
	next;
}

# struct
match($0, "^ *<struct name=\"([A-Za-z0-9_]+)\">$", A) {
	cur_type = new_type(A[1]);
	print "\ttype "cur_type" is record";
	pad_n = 0;
	context = "struct";
	next;
}

context == "struct" && match($0, "^ *<field type=\"([A-Za-z0-9_]+)\" name=\"([A-Za-z0-9_]+)\" */>$", A) {
	fld_type = type_name(A[1]);
	fld_name = identifier(A[2]);
	print "\t\t" fld_name" : "fld_type";";
	next;
}

context == "struct" && match($0, "^ *<field type=\"([A-Za-z0-9_]+)\" name=\"([A-Za-z0-9_]+)\" enum=\"([A-Za-z0-9_]+)\" */>$", A) {
	fld_type = type_name(A[1]);
	fld_name = identifier(A[2]);
	fld_enum = type_name(A[3]);
	print "\t\t" fld_name" : "fld_type"; -- Actually " fld_enum;
	next;
}

context == "struct" && match($0, "^ *<field type=\"([A-Za-z0-9_]+)\" name=\"([A-Za-z0-9_]+)\" mask=\"([A-Za-z0-9_]+)\" */>$", A) {
	fld_type = type_name(A[1]);
	fld_name = identifier(A[2]);
	fld_mask = type_name(A[3]);
	print "\t\t" fld_name" : "fld_type"; -- Actually " fld_enum;
	next;
}

context == "struct" && match($0, "^ *<pad bytes=\"([0-9]+)\" */>$", A) {
	n = A[1] + 0;
	printf "\t\t";
	while(n > 1) {
		printf "Pad"pad_n", ";
		n = n - 1;
		pad_n = pad_n + 1;
	}
	print "Pad"pad_n" : Unsigned_8;";
	pad_n = pad_n + 1;
	next;
}

match($0, "^ *</struct>$", A) {
	print "\tend record with Convention => C;";
	print "";
	context = "";
	next;
}

# enum
match($0, "^ *<enum name=\"([A-Za-z0-9_]+)\">$", A) {
	cur_type = new_type(A[1]);
	print "\tsubtype "cur_type" is C.int;";
	context = "enum";
	next;
}

context == "enum" && match($0, "^ *<item name=\"([A-Za-z0-9_]+)\"> *<value>([0-9]+)</value></item>$", A) {
	itm_name = identifier(A[1]);
	itm_value = A[2];
	print "\t"itm_name" : constant "cur_type" := "itm_value";";
	next;
}

context == "enum" && match($0, "^ *<item name=\"([A-Za-z0-9_]+)\"> *<bit>([0-9]+)</bit></item>$", A) {
	itm_name = identifier(A[1]);
	itm_value = A[2];
	print "\t"itm_name" : constant "cur_type" := 2**"itm_value";";
	next;
}

match($0, "^ *</enum>$", A) {
	print "";
	context = "";
	next;
}

# Empty string
/^[ \t]*$/ {
	next;
}

{
	print "-- ? " $0;
	unknowns = unknowns + 1;
}

END {
	if (unknowns > 0) {
		print "--  "unknowns" unknown lines (marked '?' in comments)";
		#exit 1;
	}
}