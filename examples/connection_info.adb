with XAB, XAB.Core, XAB.XProto;
with Interfaces.C;
use Interfaces;
with Ada.Text_IO;
use  Ada.Text_IO;

--
-- Ada version of an example found at https://xcb.freedesktop.org/tutorial/
-- "Checking basic information about a connection"
--
procedure Connection_Info is
	Cn : XAB.Connection_Ptr;
	Screen_Num : aliased C.int;
	Setup : access constant XAB.XProto.Setup;
	Iter : aliased XAB.XProto.Screen_Iterator;
	Screen : access XAB.XProto.Screen;
begin
	Cn := XAB.Connect (Screen => Screen_Num'Access);

	Setup := XAB.Core.Get_Setup (Cn);
	Iter := XAB.XProto.Setup_Roots_Iterator (Setup);

	for I in 1 .. Screen_Num loop
		XAB.XProto.Screen_Next (Iter'Access);
	end loop;

	Screen := Iter.Data;

	Put_Line ("Information on screen " & C.Int'Image (Screen_Num));
	Put_Line ("  width.........: " & Unsigned_16'Image (Screen.Width_in_Pixels));
	Put_Line ("  height........: " & Unsigned_16'Image (Screen.Height_in_Pixels));
	Put_Line ("  white pixel...: " & Unsigned_32'Image (Screen.White_Pixel));
	Put_Line ("  black pixel...: " & Unsigned_32'Image (Screen.Black_Pixel));

	XAB.Disconnect (Cn);

end Connection_Info;
