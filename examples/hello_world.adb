--
-- Ada version of an example found at
-- https://www.x.org/releases/X11R7.7/doc/libxcb/tutorial/index.html
-- in sction 8: Cerating a basic window - the "hello world" program
--

with XAB;
with Interfaces.C;

procedure Hello_World is
	Cn : XAB.Connection_Ptr;

	function Pause return Interfaces.C.int with Import, Convention => C, External_Name => "pause";
	
begin
	Cn := XAB.Connect;

	declare
		Dummy : Interfaces.C.int := Pause;
	begin
		null;
	end;
end Hello_World;
