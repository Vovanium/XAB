#!/usr/bin/make -f

.PHONY: proto

PROTO_XML_PATH = /usr/share/xcb/
PROTO_ADS_PATH = generated_source/

PROTO_XML = $(wildcard $(PROTO_XML_PATH)*.xml)
PROTO_ADS = $(PROTO_XML:$(PROTO_XML_PATH)%.xml=$(PROTO_ADS_PATH)xab-%.ads)

tools/build/bin/xml_to_ada : .FORCE
	cd tools; gprbuild

proto : $(PROTO_ADS)
$(PROTO_ADS): $(PROTO_ADS_PATH)xab-%.ads : $(PROTO_XML_PATH)%.xml tools/build/bin/xml_to_ada
	tools/build/bin/xml_to_ada < $^ > $@

.FORCE :
