with XAB.XProto;

--
-- All from xcb.h that depends on xproto.h
--

package XAB.Core is

	function Get_Setup (C : Connection_Ptr) return access constant XProto.Setup
	with Import, Convention => C, External_Name => "xcb_get_setup";

end XAB.Core;