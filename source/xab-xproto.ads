with Interfaces.C;
use  Interfaces;
--
-- TODO: should be replaced with auto-generated
--
package XAB.XProto is

	type Keycode is new Unsigned_8;

	type Window is new Unsigned_32;

	type Colormap is new Unsigned_32;

	type VisualID is new Unsigned_32;

	type Screen is record
		Root                  : Window;
		Default_Colormap      : Colormap;
		White_Pixel           : Unsigned_32;
		Black_Pixel           : Unsigned_32;
		Current_Input_Masks   : Unsigned_32;
		Width_in_Pixels       : Unsigned_16;
		Height_in_Pixels      : Unsigned_16;
		Width_in_Millimeters  : Unsigned_16;
		Height_in_Millimeters : Unsigned_16;
		Min_Installed_Maps    : Unsigned_16;
		Max_Installed_Maps    : Unsigned_16;
		Root_Visual           : VisualID;
		Backing_Stores        : Unsigned_8;
		Save_Unders           : Unsigned_8;
		Root_Depth            : Unsigned_8;
		Allowed_Depths_Len    : Unsigned_8;
	end record with Convention => C;

	type Screen_Iterator is record
		Data : access Screen;
		Remain : C.int;
		Index  : C.int;
	end record with Convention => C;

	procedure Screen_Next (I : access Screen_Iterator)
	with Import, Convention => C, External_Name => "xcb_screen_next";

	type Setup is record
		Status                      : Unsigned_8;
		Pad0                        : Unsigned_8;
		Protocol_Major_Version      : Unsigned_16;
		Protocol_Minor_Version      : Unsigned_16;
		Length                      : Unsigned_16;
		Release_Number              : Unsigned_32;
		Resource_ID_Base            : Unsigned_32;
		Resource_ID_Mask            : Unsigned_32;
		Motion_Buffer_Size          : Unsigned_32;
		Vendor_Len                  : Unsigned_32;
		Maximum_Request_Length      : Unsigned_16;
		Roots_Len                   : Unsigned_8;
		Pixmap_Fornats_Len          : Unsigned_8;
		Image_Byte_Order            : Unsigned_8;
		Bitmap_Format_Bit_Order     : Unsigned_8;
		Bitmap_Format_Scanline_Unit : Unsigned_8;
		Bitmap_Format_Scanline_Pad  : Unsigned_8;
		Min_Keycode                 : Keycode;
		Max_Keycode                 : Keycode;
		Pad1                        : Pad_Array (1 .. 4);
	end record with Convention => C;

	function Setup_Roots_Iterator (R : access constant Setup) return Screen_Iterator
	with Import, Convention => C, External_Name => "xcb_setup_roots_iterator";

end XAB.XProto;