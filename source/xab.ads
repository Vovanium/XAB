with Interfaces.C.Strings;
use  Interfaces;

--
-- X Ada Binding root
--

package XAB with Preelaborate is

	subtype INT8 is Integer_8;
	subtype INT16 is Integer_16;
	subtype INT32 is Integer_32;
	subtype CARD8 is Unsigned_8;
	subtype CARD16 is Unsigned_16;
	subtype CARD32 is Unsigned_32;

	type Pad_Array is array (Natural range <>) of Unsigned_8;

	-- Opaque type for all that need to interact with the server
	type Connection is private;

	-- Pointer to connection, the commonly used handle
	type Connection_Ptr is access all Connection;

	-- Container for authorization information
	type Auth_Info is record
		NameLen : C.int;
		Name    : C.Strings.chars_ptr;
		DataLen : C.int;
		Data    : access C.plain_char;
	end record
	with Convention => C;

	function Connect (
		Display : C.Strings.chars_ptr := C.Strings.Null_Ptr;
		Screen  : access C.int := null)
		return    Connection_Ptr
	with Import, Convention => C, External_Name => "xcb_connect";

	function Connect_to_Display_with_Auth_Info (
		Display : C.Strings.chars_ptr := C.Strings.Null_Ptr;
		Auth    : access Auth_Info;
		Screen  : access C.int := null)
		return    Connection_Ptr
	with Import, Convention => C, External_Name => "xcb_connect_to_display_with_auth_info";

	function Connect_to_FD (
		FD      : C.int;
		Auth    : access Auth_Info := null)
		return    Connection_Ptr
	with Import, Convention => C, External_Name => "xcb_connect_to_fd";

	function Parse_Display (
		Name    : C.Strings.chars_ptr := C.Strings.Null_Ptr;
		Host    : not null access C.Strings.chars_ptr;
		Display : not null access C.int;
		Screen  : access C.int := null)
		return    C.int
	with Import, Convention => C, External_Name => "xcb_parse_display";

	function Get_File_Descriptor (Con : Connection_Ptr) return C.int
	with Import, Convention => C, External_Name => "xcb_get_file_descriptor";

	function Connection_Has_Error (Con : Connection_Ptr) return C.int
	with Import, Convention => C, External_Name => "xcb_connection_has_error";

	procedure Disconnect (Con : Connection_Ptr)
	with Import, Convention => C, External_Name => "xcb_disconnect";

	procedure Flush (Con : Connection_Ptr)
	with Import, Convention => C, External_Name => "xcb_flush";

	function Get_Maximun_Request_Length (C : Connection_Ptr) return Unsigned_32
	with Import, Convention => C, External_Name => "xcb_get_maximum_request_length";

	procedure Prefetch_Maximum_Request_Length (C : Connection_Ptr)
	with Import, Convention => C, External_Name => "xcb_prefetch_maximum_request_length";

	function Generate_ID (C : Connection_Ptr) return Unsigned_32
	with Import, Convention => C, External_Name => "xcb_generate_id";

private
	type Connection is null record;
end XAB;
